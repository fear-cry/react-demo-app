/* eslint-disable no-bitwise */
export default function simpleHash(str) {
    let hash = 0;

    for (let i = 0; i < str.length; i += 1) {
        const char = str.charCodeAt(i);
        hash = (hash << 5) - hash + char;
        hash &= hash; // Convert to 32bit integer
    }
    return new Uint32Array([hash])[0].toString(36);
}
