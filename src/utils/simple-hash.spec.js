import simpleHash from './simple-hash';

describe('simpleHash', () => {
    it('long string', () => {
        expect(
            simpleHash(
                'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolorem, qui quidem quam sapiente porro ad blanditiis, sed odio explicabo quisquam eaque, obcaecati doloremque aperiam? Beatae voluptates itaque sequi minima voluptate!',
            ),
        ).toBe('1fdlonk');
    });

    it('short string', () => {
        expect(simpleHash('abc')).toBe('22ci');
    });

    it('empty string', () => {
        expect(simpleHash('')).toBe('0');
    });

    it('number', () => {
        expect(simpleHash(123)).toBe('0');
    });

    it('Throw error when undefuned arg', () => {
        expect(() => simpleHash(undefined)).toThrowError();
    });

    it('Throw error when null arg', () => {
        expect(() => simpleHash(null)).toThrowError();
    });
});
