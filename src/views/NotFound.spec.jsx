import React from 'react';
import { render } from '@testing-library/react';

import NotFound from './NotFound';

describe('<NotFound />', () => {
    it('Renders <NotFound /> component correctly', () => {
        const { getByText } = render(<NotFound />);

        expect(getByText(/Not found/i)).toBeInTheDocument();
    });
});
