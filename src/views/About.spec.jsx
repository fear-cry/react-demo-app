import React from 'react';
import { render } from '@testing-library/react';

import About from './About';

describe('<About />', () => {
    it('Renders <About /> component correctly', () => {
        const { getByText } = render(<About />);

        expect(getByText(/About page/i)).toBeInTheDocument();
    });
});
