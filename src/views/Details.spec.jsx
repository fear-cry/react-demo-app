import React from 'react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import {
    render,
    fireEvent,
    screen,
    act,
    wait,
} from '@/test/test-utils';

import Details from '@/views/Details';

// We use msw to intercept the network request during the test,
// and return the response 'John Smith' after 150ms
// when receiving a get request to the `/api/user` endpoint
const handlers = [
    rest.get(
        'https://the-one-api.dev/v2/character/333',
        (req, res, ctx) =>
            res(
                ctx.json({ docs: [{ _id: 333, race: 'foo' }] }),
                ctx.delay(150),
            ),
    ),
];

const server = setupServer(...handlers);

// Enable API mocking before tests.
beforeAll(() => server.listen());

// Reset any runtime request handlers we may add during the tests.
afterEach(() => server.resetHandlers());

// Disable API mocking after the tests are done.
afterAll(() => server.close());

test.skip('fetches & receives data after clicking the button', async () => {
    const match = { params: { id: 333 } };
    const setIsLoading = jest.fn();

    await act(async () => {
        await render(
            <Details match={match} setIsLoading={setIsLoading} />,
        );
    });
});
