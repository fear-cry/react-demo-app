import React from 'react';

export default function Header() {
    return (
        <div className="container">
            <div className="mt-5 text-center">
                <h1 className="mb-5">About page</h1>
                <p>
                    Lorem, ipsum dolor sit amet consectetur
                    adipisicing elit. Tempore magni odio alias
                    tempora, at deserunt! Nostrum delectus veniam
                    tenetur quidem in magni, ratione aliquid voluptas
                    atque dolorem corporis, omnis minima.
                </p>
            </div>
        </div>
    );
}
