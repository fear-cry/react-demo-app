import React from 'react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { render, screen, act, fireEvent } from '@/test/test-utils';

import Home from '@/views/Home';

const handlers = [
    rest.get(
        'https://the-one-api.dev/v2/character',
        (req, res, ctx) =>
            res(
                ctx.json({
                    docs: [{ _id: 333, name: 'Albert', race: 'foo' }],
                }),
                ctx.delay(150),
            ),
    ),
];

const server = setupServer(...handlers);

// Enable API mocking before tests.
beforeAll(() => server.listen());

// Reset any runtime request handlers we may add during the tests.
afterEach(() => server.resetHandlers());

// Disable API mocking after the tests are done.
afterAll(() => server.close());

describe('Home', () => {
    const SUBMIT_BUTTON_ID = 'submit';
    const SEARCH_FIELD_ID = 'search';

    it('good render', () => {
        render(<Home />);

        expect(screen.getByTestId('logo')).toBeInTheDocument();
    });

    it.skip('render list', async () => {
        render(<Home />);

        const inputEl = screen.getByTestId(SEARCH_FIELD_ID);
        const searchButton = screen.getByTestId(SUBMIT_BUTTON_ID);

        fireEvent.change(inputEl, { target: { value: '333' } });

        await act(async () => {
            await fireEvent.click(searchButton);
        });
    });
});
