import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actions } from '@/store/reducers/characterItem';

function mapState({ characterItem }) {
    return characterItem;
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

function Details({ match, result, errors, fetchItem, setErrors }) {
    const { id } = match.params;
    const [isLoading, setIsLoading] = useState(false);

    const getData = async () => {
        try {
            setIsLoading(true);
            await fetchItem(id);
        } catch (error) {
            console.error(error);
        } finally {
            setIsLoading(false);
        }

        if (result.errors) {
            setErrors(result.errors);
        }
    };

    useEffect(() => {
        getData();
    }, []);

    const filterField = (value = '') => value.replace('NaN', '');

    const character = result.docs?.[0] || {};

    const regextLink = /^https?:\/\//;

    const viewAllFields = (item) =>
        Object.keys(item)
            .filter((x) => item[x] && x !== '_id')
            .map((key) => {
                let value = filterField(item[key]);

                if (regextLink.test(value)) {
                    value = (
                        <a
                            href={value}
                            target="_blank"
                            rel="noreferrer"
                        >
                            {value}
                        </a>
                    );
                }

                return (
                    <p key={key}>
                        <strong>{key.toUpperCase()}: </strong> {value}
                    </p>
                );
            });

    return (
        <div className="container">
            <h2 className="my-5">Full details about character</h2>

            {isLoading ? (
                <div className="d-block text-center mt-5">
                    <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            ) : null}

            {!isLoading && Object.keys(errors).length
                ? Object.keys(errors).map((key) => (
                      <p className="color-danger" key={key}>
                          {errors[key]}
                      </p>
                  ))
                : null}

            {!isLoading && !Object.keys(errors).length ? (
                <div className="search-result__card card mb-3 overflow-hidden shadow-sm">
                    <div className="card-body d-flex">
                        <div className="d-block ml-4">
                            {viewAllFields(character)}
                        </div>
                    </div>
                </div>
            ) : null}
        </div>
    );
}

export default connect(mapState, mapDispatchToProps)(Details);
