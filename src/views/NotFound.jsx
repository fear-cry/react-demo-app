import React from 'react';

export default function NotFound() {
    return (
        <div className="container">
            <div className="mt-5 text-center">
                <h1 className="mb-5">404</h1>
                <h2>Not found</h2>
            </div>
        </div>
    );
}
