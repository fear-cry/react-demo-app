import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actions } from '@/store/reducers/characterList';
import CardList from '@/components/CardList';
import SortBy from '@/components/SortBy';
import Paging from '@/components/Paging';
import Form from '@/components/Form';

import 'bootstrap/dist/css/bootstrap.min.css';
import '@/styles/app.scss';

function mapState({ characterList }) {
    return characterList;
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

function Home({
    result,
    query,
    sortBy,
    errors,
    page,
    pageSize,
    fetchList,
    setQuery,
    setSortBy,
    setErrors,
    setPage,
    setPageSize,
}) {
    const [isLoading, setIsLoading] = useState(false);

    const search = async () => {
        setErrors({});
        setIsLoading(true);

        try {
            await fetchList({ query, sortBy, page, pageSize });
        } catch (error) {
            console.error(error);
        } finally {
            setIsLoading(false);
        }

        if (result.errors) {
            setErrors(result.errors);
        }
    };

    useEffect(() => {
        if (result.docs) {
            search();
        }
    }, [sortBy, page, pageSize]);

    return (
        <div className="page">
            <div className="search-wrapper header">
                <img
                    src="https://www.freepngimg.com/thumb/lord_of_the_rings/23904-2-lord-of-the-rings-logo-file.png"
                    className="header__logo"
                    alt="logo"
                    data-testid="logo"
                />

                <Form
                    search={search}
                    setQuery={setQuery}
                    errors={errors}
                    query={query}
                />

                <SortBy setSortBy={setSortBy} sortBy={sortBy} />
            </div>

            <div className="container">
                <div className="d-flex flex-column mt-4">
                    <div className="w-50 mb-4">
                        <Paging
                            setPage={setPage}
                            setPageSize={setPageSize}
                            total={result.total}
                            search={search}
                            error={errors.paging}
                        />
                    </div>

                    {!isLoading ? (
                        <CardList results={result?.docs || []} />
                    ) : null}

                    {isLoading ? (
                        <div className="d-block text-center">
                            <div
                                className="spinner-border"
                                role="status"
                            >
                                <span className="sr-only">
                                    Loading...
                                </span>
                            </div>
                        </div>
                    ) : null}
                </div>
            </div>
        </div>
    );
}

export default connect(mapState, mapDispatchToProps)(Home);
