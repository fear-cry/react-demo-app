import axios from 'axios';
import { setupCache } from 'axios-cache-adapter';

const cache = setupCache({
    maxAge: 15 * 60 * 1000,
    exclude: { query: false },
});

const API_TOKEN = 'iBpiWhA8YoFuR94SHHK-';

const axiosInstance = axios.create({
    baseURL: 'https://the-one-api.dev/v2',
    timeout: 1000,
    headers: { Authorization: `Bearer ${API_TOKEN}` },
    adapter: cache.adapter,
});

export default axiosInstance;
