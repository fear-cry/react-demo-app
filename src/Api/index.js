/* eslint-disable no-unreachable */
import axios from './axios';

export default {
    async getCharacters(query, sortBy, page, pageSize) {
        const pageParams = page || 1;
        const pageSizeParams = pageSize || 10;

        if (!query) {
            return {
                errors: {
                    search: 'Please type anything',
                },
            };
        }

        let res;

        try {
            res = await axios.get(
                `character?name=/${query}/i&sort=${sortBy}:asc&page=${pageParams}&limit=${pageSizeParams}`,
            );
        } catch (error) {
            if (error.response && error.response.status === 429) {
                return {
                    errors: {
                        search: 'Too Many Requests',
                    },
                };
            }

            // console.log(error);
            return {};
        }

        if (res.status === 200 && res.data.docs) {
            if (res.data.docs?.length === 0) {
                return {
                    docs: [],
                    errors: {
                        search: 'Not found',
                    },
                };
            }

            return res.data;
        }

        return {};
    },

    async getCharacter(id) {
        if (!id) {
            return {
                errors: {
                    message: 'Wrong id!',
                },
            };
        }

        let res;

        try {
            res = await axios.get(`character/${id}`);
        } catch (error) {
            if (error.response?.status === 429) {
                return {
                    errors: {
                        search: 'Too Many Requests',
                    },
                };
            }

            if (error.response?.status === 500) {
                return {
                    docs: [],
                    errors: {
                        message: 'Not found',
                    },
                };
            }

            // console.log(error);
            return {};
        }

        if (res.status === 200 && res.data.docs) {
            if (res.data.docs?.length === 0) {
                return {
                    docs: [],
                    errors: {
                        message: 'Not found',
                    },
                };
            }

            return res.data;
        }

        return {};
    },
};
