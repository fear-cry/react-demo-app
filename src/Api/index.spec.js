import { rest } from 'msw';
import { setupServer } from 'msw/node';
import api from './index';

const mockResponse = { data: {}, status: 200 };

const handlers = [
    rest.get('*', (req, res, ctx) =>
        res(ctx.json(mockResponse.data), ctx.delay(50)),
    ),
];

const server = setupServer(...handlers);

// Enable API mocking before tests.
beforeAll(() => server.listen());

// Reset any runtime request handlers we may add during the tests.
afterEach(() => {
    server.resetHandlers();
    mockResponse.data = {};
});

// Disable API mocking after the tests are done.
afterAll(() => server.close());

describe('api', () => {
    it('getCharacters', async () => {
        const query = '1';
        const sortBy = 'name';
        const page = 4;
        const pageSize = 1;
        const result = { docs: [{ _id: 333, race: 'foo' }] };
        mockResponse.data = result;

        const res = await api.getCharacters(
            query,
            sortBy,
            page,
            pageSize,
        );

        expect(res).toEqual(result);
    });

    it('getCharacters Please type anything', async () => {
        const query = '';
        const sortBy = 'name';
        const page = 4;
        const pageSize = 1;
        const result = {
            errors: { search: 'Please type anything' },
        };
        mockResponse.data = result;

        const res = await api.getCharacters(
            query,
            sortBy,
            page,
            pageSize,
        );

        expect(res).toEqual(result);
    });

    it('getCharacters Not found', async () => {
        const query = 'notfound';
        const sortBy = 'name';
        const page = 4;
        const pageSize = 1;
        const result = {
            docs: [],
            errors: { search: 'Not found' },
        };
        mockResponse.data = { docs: [] };

        const res = await api.getCharacters(
            query,
            sortBy,
            page,
            pageSize,
        );

        expect(res).toEqual(result);
    });

    it('getCharacters Too Many Requests', async () => {
        const query = 'notfound2';
        const sortBy = 'name';
        const page = 4;
        const pageSize = 1;
        const result = {
            errors: { search: 'Too Many Requests' },
        };

        const networkErrorHandlers = [
            rest.get('*', (req, res, ctx) => res(ctx.status(429))),
        ];

        server.use(...networkErrorHandlers);

        const res = await api.getCharacters(
            query,
            sortBy,
            page,
            pageSize,
        );

        expect(res).toEqual(result);
    });

    it('getCharacters any error', async () => {
        const query = 'notfound2';
        const sortBy = 'name';
        const page = 4;
        const pageSize = 1;
        const result = {};
        const networkErrorHandlers = [
            rest.get('*', (req, res, ctx) => res(ctx.status(503))),
        ];

        server.use(...networkErrorHandlers);

        const res = await api.getCharacters(
            query,
            sortBy,
            page,
            pageSize,
        );

        expect(res).toEqual(result);
    });

    it('getCharacters no page, no pageSize', async () => {
        const query = 'notfound2';
        const sortBy = 'name';
        const result = {};

        const res = await api.getCharacters(query, sortBy);

        expect(res).toEqual(result);
    });

    it('getCharacters any error 2', async () => {
        const query = 'any';
        const sortBy = 'name';
        const page = 4;
        const pageSize = 1;
        const result = {};
        const networkErrorHandlers = [
            rest.get('*', (req, res, ctx) => res(ctx.status(201))),
        ];

        server.use(...networkErrorHandlers);

        const res = await api.getCharacters(
            query,
            sortBy,
            page,
            pageSize,
        );

        expect(res).toEqual(result);
    });

    it('getCharacter Too Many Requests', async () => {
        const id = 3;
        const result = {
            errors: { search: 'Too Many Requests' },
        };
        const networkErrorHandlers = [
            rest.get('*', (req, res, ctx) => res(ctx.status(429))),
        ];

        server.use(...networkErrorHandlers);

        const res = await api.getCharacter(id);

        expect(res).toEqual(result);
    });

    it('getCharacter not found 2', async () => {
        const id = 3;
        const result = {
            docs: [],
            errors: { message: 'Not found' },
        };
        const networkErrorHandlers = [
            rest.get('*', (req, res, ctx) => res(ctx.status(500))),
        ];

        server.use(...networkErrorHandlers);

        const res = await api.getCharacter(id);

        expect(res).toEqual(result);
    });

    it('getCharacter any error', async () => {
        const id = 3;
        const result = {};
        const networkErrorHandlers = [
            rest.get('*', (req, res, ctx) => res(ctx.status(503))),
        ];

        server.use(...networkErrorHandlers);

        const res = await api.getCharacter(id);

        expect(res).toEqual(result);
    });

    it('getCharacter empty doc', async () => {
        const id = 3;
        const result = {};
        const networkErrorHandlers = [
            rest.get('*', (req, res, ctx) => res(ctx.status(201))),
        ];

        server.use(...networkErrorHandlers);

        const res = await api.getCharacter(id);

        expect(res).toEqual(result);
    });

    it('getCharacter', async () => {
        const id = '1';
        const result = { docs: [{ _id: 333, race: 'foo' }] };
        mockResponse.data = result;

        const res = await api.getCharacter(id);

        expect(res).toEqual(result);
    });

    it('getCharacter not found', async () => {
        const id = '2';
        const result = {
            docs: [],
            errors: { message: 'Not found' },
        };
        mockResponse.data = { docs: [] };

        const res = await api.getCharacter(id);

        expect(res).toEqual(result);
    });

    it('getCharacter wrong id', async () => {
        const result = {
            errors: { message: 'Wrong id!' },
        };
        mockResponse.data = { docs: [] };

        const res = await api.getCharacter();

        expect(res).toEqual(result);
    });
});
