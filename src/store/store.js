import { configureStore } from '@reduxjs/toolkit';
import characterList from '@/store/reducers/characterList';
import characterItem from '@/store/reducers/characterItem';

const store = configureStore({
    reducer: { characterList, characterItem },
    devTools: process.env.NODE_ENV !== 'production',
});

export default store;
