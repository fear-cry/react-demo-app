/* eslint-disable no-param-reassign */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import api from '@/Api';

const initialState = {
    result: {},
    errors: {},
};

const fetchItem = createAsyncThunk('characterItem/fetch', (id) =>
    api.getCharacter(id),
);

const characterItemSlice = createSlice({
    name: 'characterItem',
    initialState,
    reducers: {
        setErrors: (state, action) => {
            state.errors = action.payload;
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchItem.fulfilled, (state, action) => {
            state.result = { ...action.payload };
        });
    },
});

export const actions = { ...characterItemSlice.actions, fetchItem };

export default characterItemSlice.reducer;
