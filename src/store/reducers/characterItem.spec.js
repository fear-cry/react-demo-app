import reducer, { actions } from './characterItem';

const { setErrors } = actions;

describe('reducers characterItem', () => {
    const previousState = {
        result: {},
        errors: {},
    };

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(previousState);
    });

    it('should handle set errors', () => {
        const errors = {
            search: 'Too Many Requests',
        };

        expect(reducer(previousState, setErrors(errors))).toEqual({
            result: {},
            errors: errors,
        });
    });
});
