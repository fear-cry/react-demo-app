import reducer, { actions } from './characterList';

const { setQuery, setSortBy, setErrors, setPage, setPageSize } =
    actions;

describe('reducers characterList', () => {
    const previousState = {
        result: {},
        query: '',
        sortBy: 'name',
        errors: {},
        page: 1,
        pageSize: 10,
    };

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(previousState);
    });

    it('should handle set errors', () => {
        const errors = {
            search: 'Too Many Requests',
        };

        expect(reducer(previousState, setErrors(errors))).toEqual({
            ...previousState,
            errors: errors,
        });
    });

    it('should handle setQuery', () => {
        const query = 'foo';

        expect(reducer(previousState, setQuery(query))).toEqual({
            ...previousState,
            query: query,
        });
    });

    it('should handle setSortBy', () => {
        const sortBy = 'foo';

        expect(reducer(previousState, setSortBy(sortBy))).toEqual({
            ...previousState,
            sortBy: sortBy,
        });
    });

    it('should handle setPage', () => {
        const page = 8;

        expect(reducer(previousState, setPage(page))).toEqual({
            ...previousState,
            page: page,
        });
    });

    it('should handle setPageSize', () => {
        const pageSize = 5;

        expect(reducer(previousState, setPageSize(pageSize))).toEqual(
            {
                ...previousState,
                pageSize: pageSize,
            },
        );
    });
});
