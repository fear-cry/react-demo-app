/* eslint-disable no-param-reassign */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import api from '@/Api';

const initialState = {
    result: {},
    query: '',
    sortBy: 'name',
    errors: {},
    page: 1,
    pageSize: 10,
};

const fetchList = createAsyncThunk(
    'characterList/fetch',
    ({ query, sortBy, page, pageSize }) =>
        api.getCharacters(query, sortBy, page, pageSize),
);

const characterListSlice = createSlice({
    name: 'characterList',
    initialState,
    reducers: {
        setQuery: (state, action) => {
            state.query = action.payload;
        },
        setSortBy: (state, action) => {
            state.sortBy = action.payload;
        },
        setErrors: (state, action) => {
            state.errors = action.payload;
        },
        setPage: (state, action) => {
            state.page = action.payload;
        },
        setPageSize: (state, action) => {
            state.pageSize = action.payload;
        },
    },
    extraReducers: (builder) => {
        builder.addCase(fetchList.fulfilled, (state, action) => {
            state.result = { ...action.payload };
        });
    },
});

export const actions = { ...characterListSlice.actions, fetchList };

export default characterListSlice.reducer;
