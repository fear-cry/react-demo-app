import React from 'react';
import Header from '@/components/Header';
import { Route, BrowserRouter } from 'react-router-dom';
import Home from '@/views/Home';
import About from '@/views/About';
import Details from '@/views/Details';
import NotFound from '@/views/NotFound';
import { CSSTransition } from 'react-transition-group';
import Switch from 'react-router-transition-switch';
import Fader from 'react-fader';

const routes = [
    { path: '/', name: 'Home', Component: Home },
    { path: '/about', name: 'About', Component: About },
];

export default function App() {
    return (
        <BrowserRouter>
            <div>
                <Header routes={routes} />

                <div className="position-relative">
                    <Switch
                        key={window.location.key}
                        location={window.location}
                        component={Fader}
                    >
                        {routes.map(({ path, Component }) => (
                            <Route key={path} exact path={path}>
                                {({ match }) => (
                                    <CSSTransition
                                        in={match != null}
                                        timeout={300}
                                        classNames="animate"
                                        unmountOnExit
                                    >
                                        <div className="animate">
                                            <Component />
                                        </div>
                                    </CSSTransition>
                                )}
                            </Route>
                        ))}

                        <Route key="details" path="/details/:id">
                            {({ match }) => (
                                <CSSTransition
                                    in={match !== null}
                                    timeout={300}
                                    classNames="animate"
                                    unmountOnExit
                                >
                                    <div className="animate">
                                        <Details match={match} />
                                    </div>
                                </CSSTransition>
                            )}
                        </Route>

                        <Route>
                            {({ match }) => (
                                <CSSTransition
                                    in={match !== null}
                                    timeout={300}
                                    classNames="animate"
                                    unmountOnExit
                                >
                                    <div className="animate">
                                        <NotFound />
                                    </div>
                                </CSSTransition>
                            )}
                        </Route>
                    </Switch>
                </div>
            </div>
        </BrowserRouter>
    );
}
