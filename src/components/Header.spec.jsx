import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

import Header from './Header';

describe('<Header />', () => {
    const routes = [
        { path: '/', name: 'Home' },
        { path: '/about', name: 'About' },
    ];

    it('Renders <Header /> component correctly', () => {
        render(
            <MemoryRouter>
                <Header routes={routes} />
            </MemoryRouter>,
        );

        expect(
            screen.getByText(/React.routing/i),
        ).toBeInTheDocument();
    });
});
