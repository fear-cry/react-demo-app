import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Form from './Form';

describe('<Form />', () => {
    const SEARCH_FIELD_ID = 'search';
    const SUBMIT_BUTTON_ID = 'submit';
    const ERROR_ID = 'error';

    it('Renders <Form /> component correctly', () => {
        const errors = {};
        render(<Form errors={errors} />);

        expect(
            screen.getByTestId(SEARCH_FIELD_ID),
        ).toBeInTheDocument();
    });

    it('check submit', () => {
        const errors = {};
        const search = jest.fn();
        render(<Form errors={errors} search={search} />);

        const submitButtonEl = screen.getByTestId(SUBMIT_BUTTON_ID);
        userEvent.click(submitButtonEl);

        expect(screen.queryByTestId(ERROR_ID)).toBe(null);
        expect(search).toBeCalled();
    });

    it('Error after submit', () => {
        const errors = { search: 'ERROR' };
        render(<Form errors={errors} />);

        expect(screen.getByTestId(ERROR_ID)).toBeInTheDocument();
    });

    it('Type', () => {
        const errors = { search: 'ERROR' };
        const setQuery = jest.fn();
        render(<Form errors={errors} setQuery={setQuery} />);

        const TEXT_EXAMPLE = 'foo bar';

        const inputEl = screen.getByTestId(SEARCH_FIELD_ID);
        userEvent.clear(inputEl);
        userEvent.type(inputEl, TEXT_EXAMPLE);

        expect(screen.getByTestId(SEARCH_FIELD_ID)).toHaveValue(
            TEXT_EXAMPLE,
        );
    });
});
