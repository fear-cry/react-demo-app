import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

import CardList from './CardList';

describe('CardList', () => {
    it('list of elements', () => {
        const results = [
            {
                _id: '1',
                name: 'name lorem',
                race: 'race ipsum',
                spouse: 'spouse lorem',
                gender: 'gender ipsum',
                birth: 'birth 13-13-2099',
            },
        ];

        render(
            <MemoryRouter>
                <CardList results={results} />
            </MemoryRouter>,
        );

        expect(screen.getByText(/name lorem/i)).toBeInTheDocument();
        expect(screen.getByText(/race ipsum/i)).toBeInTheDocument();
        expect(screen.getByText(/spouse lorem/i)).toBeInTheDocument();
        expect(screen.getByText(/gender ipsum/i)).toBeInTheDocument();
        expect(
            screen.getByText(/birth 13-13-2099/i),
        ).toBeInTheDocument();
    });

    it('not render name with NaN string', () => {
        const results = [
            {
                _id: '1',
                name: 'NaN',
            },
        ];

        render(
            <MemoryRouter>
                <CardList results={results} />
            </MemoryRouter>,
        );

        expect(document.body).not.toHaveTextContent('NaN');
    });
});
