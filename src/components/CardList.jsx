import React from 'react';
import { NavLink } from 'react-router-dom';

export default function CardList(props) {
    const { results } = props;

    const filterField = (value = '') => value.replace('NaN', '');

    return (
        <div className="search-result">
            {results.map((character) => {
                // eslint-disable-next-line no-underscore-dangle
                const id = character._id;

                return (
                    <NavLink
                        to={`details/${id}`}
                        key={id}
                        className="search-result__card card mb-3 overflow-hidden shadow-sm"
                    >
                        <div className="card-body d-flex">
                            <div className="d-block ml-4">
                                <p>
                                    <strong>Name: </strong>{' '}
                                    {filterField(character.name)}
                                </p>

                                <p>
                                    <strong>Race: </strong>{' '}
                                    {filterField(character.race)}
                                </p>

                                <p>
                                    <strong>Spouse: </strong>{' '}
                                    {filterField(character.spouse)}
                                </p>

                                <p>
                                    <strong>Gender: </strong>{' '}
                                    {filterField(character.gender)}
                                </p>

                                <p>
                                    <strong>birth: </strong>{' '}
                                    {filterField(character.birth)}
                                </p>
                            </div>
                        </div>
                    </NavLink>
                );
            })}
        </div>
    );
}
