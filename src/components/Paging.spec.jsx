import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Paging from './Paging';

describe('<Paging />', () => {
    const INPUT_PAGE_ID = 'input-page';
    const INPUT_PAGE_SIZE_ID = 'input-page-size';
    const SUBMIT_BUTTON_ID = 'submit-button';

    it('Renders <Paging /> with input page field', () => {
        render(<Paging />);

        const inputEl = screen.getByTestId(INPUT_PAGE_ID);
        expect(inputEl).toBeInTheDocument();
        expect(inputEl).toHaveAttribute('type', 'text');
    });

    it('type into page field', () => {
        render(<Paging />);

        const TEXT_EXAMPLE = '31';

        const inputEl = screen.getByTestId(INPUT_PAGE_ID);
        userEvent.clear(inputEl);
        userEvent.type(inputEl, TEXT_EXAMPLE);

        expect(screen.getByTestId(INPUT_PAGE_ID)).toHaveValue(
            TEXT_EXAMPLE,
        );
    });

    it('show error message for page field', () => {
        render(<Paging />);

        const TEXT_EXAMPLE = '0';

        const inputEl = screen.getByTestId(INPUT_PAGE_ID);
        userEvent.clear(inputEl);
        userEvent.type(inputEl, TEXT_EXAMPLE);

        expect(screen.getByTestId(INPUT_PAGE_ID)).toHaveValue(
            TEXT_EXAMPLE,
        );

        expect(
            screen.getByText('Page must be more then 0!'),
        ).toBeInTheDocument();
    });

    it('total props', () => {
        render(<Paging total={12} />);

        const TEXT_EXAMPLE = '2';

        const inputEl = screen.getByTestId(INPUT_PAGE_SIZE_ID);
        userEvent.clear(inputEl);
        userEvent.type(inputEl, TEXT_EXAMPLE);

        expect(screen.getByText(/Total pages/i)).toBeInTheDocument();
    });

    it('show error message for page size field', () => {
        render(<Paging />);

        const inputEl = screen.getByTestId(INPUT_PAGE_ID);
        userEvent.clear(inputEl);

        expect(
            screen.getByText(/Page is required!/),
        ).toBeInTheDocument();
    });

    it('show error message for page size field', () => {
        render(<Paging />);

        const inputEl = screen.getByTestId(INPUT_PAGE_SIZE_ID);
        userEvent.clear(inputEl);

        expect(
            screen.getByText('Page size is required!'),
        ).toBeInTheDocument();
    });

    it('stop when type a not number', () => {
        render(<Paging />);

        const TEXT_EXAMPLE = 'abc';

        const inputEl = screen.getByTestId(INPUT_PAGE_ID);
        userEvent.clear(inputEl);
        userEvent.type(inputEl, TEXT_EXAMPLE);

        expect(screen.getByTestId(INPUT_PAGE_ID)).not.toHaveValue(
            TEXT_EXAMPLE,
        );
    });

    it('submit form without error', () => {
        const setPage = jest.fn();
        const setPageSize = jest.fn();

        render(
            <Paging setPage={setPage} setPageSize={setPageSize} />,
        );
        const submitButtonEl = screen.getByTestId(SUBMIT_BUTTON_ID);
        userEvent.click(submitButtonEl);

        expect(setPage).toBeCalled();
        expect(setPageSize).toBeCalled();
    });

    it('submit with error', () => {
        const setPage = jest.fn();
        const setPageSize = jest.fn();

        render(
            <Paging setPage={setPage} setPageSize={setPageSize} />,
        );

        const TEXT_EXAMPLE = 'abc';

        const inputEl = screen.getByTestId(INPUT_PAGE_ID);
        userEvent.clear(inputEl);
        userEvent.type(inputEl, TEXT_EXAMPLE);

        const submitButtonEl = screen.getByTestId(SUBMIT_BUTTON_ID);
        userEvent.click(submitButtonEl);

        expect(setPage).not.toBeCalled();
        expect(setPageSize).not.toBeCalled();
    });
});
