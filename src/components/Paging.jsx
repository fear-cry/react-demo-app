import React, { useState, memo, useEffect } from 'react';

function Paging(props) {
    const { setPage, setPageSize, total, error } = props;

    const [form, setForm] = useState({
        page: '1',
        pageSize: '10',
    });

    const validators = {
        page(val) {
            if (val.trim() === '') {
                return 'Page is required!';
            }

            if (val < 1) {
                return 'Page must be more then 0!';
            }

            return false;
        },

        pageSize(val) {
            if (val.trim() === '') {
                return 'Page size is required!';
            }

            return false;
        },
    };

    const [errors, setErrors] = useState({});

    const checkInputNumber = (value) => {
        const regexpNumber = /^(\d+|)$/g;

        return regexpNumber.test(value);
    };

    const validateForm = () => {
        const newErrors = {};

        setErrors(newErrors);

        Object.keys(validators).forEach((field) => {
            const inputError = validators[field]?.(form[field]);

            if (inputError) {
                newErrors[field] = inputError;
            }
        });

        setErrors(newErrors);

        return !!Object.keys(newErrors).length;
    };

    const handleInputChange = (event) => {
        const { target } = event;
        const { value } = target;
        const { name } = target;

        if (!checkInputNumber(value)) {
            return;
        }

        setForm({
            ...form,
            [name]: value,
        });
    };

    useEffect(() => {
        validateForm();
    }, [form]);

    const emitFormValues = () => {
        setPage(form.page);
        setPageSize(form.pageSize);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        e.stopPropagation();

        emitFormValues();
    };

    const getError = (field) => errors[field];
    const hasErrors = () => Object.keys(errors).length;

    const getErrorHtml = (field) => {
        const errorText = getError(field);

        return errorText ? (
            <div className="absolute -bottom-24 left-0">
                <small className="color-danger">{errorText}</small>
            </div>
        ) : null;
    };

    return (
        <form onSubmit={handleSubmit}>
            <div className="form-row w-100">
                <div className="col-md-4">
                    <strong className="">Pagination</strong>

                    {form.pageSize && total ? (
                        <p className="mb-0 mt-2">
                            Total pages:{' '}
                            {Math.floor(total / form.pageSize)}
                        </p>
                    ) : null}
                </div>

                <div className="col-md-3">
                    <label htmlFor="page">
                        Page
                        <input
                            className={`form-control ${
                                error || getError('page')
                                    ? 'is-invalid'
                                    : ''
                            }`}
                            type="text"
                            name="page"
                            id="page"
                            onChange={handleInputChange}
                            value={form.page}
                            data-testid="input-page"
                        />
                    </label>

                    {getErrorHtml('page')}
                </div>

                <div className="col-md-3">
                    <label htmlFor="pageSize">
                        Limit
                        <input
                            className={`form-control ${
                                error || getError('pageSize')
                                    ? 'is-invalid'
                                    : ''
                            }`}
                            type="text"
                            name="pageSize"
                            id="pageSize"
                            onChange={handleInputChange}
                            value={form.pageSize}
                            data-testid="input-page-size"
                        />
                    </label>

                    {getErrorHtml('pageSize')}
                </div>

                <div className="col-md-2">
                    <button
                        type="submit"
                        className="btn btn-success btn-block mr-3 mt-4"
                        disabled={hasErrors()}
                        data-testid="submit-button"
                    >
                        Apply
                    </button>
                </div>
            </div>
        </form>
    );
}

export default memo(Paging);
