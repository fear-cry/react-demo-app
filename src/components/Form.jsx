import React from 'react';

export default function Form(props) {
    const { errors, search, query, setQuery } = props;

    const handleSubmit = async (e) => {
        e.preventDefault();

        search();
    };

    const handleChange = (e) => {
        setQuery(e.target.value);
    };

    return (
        <form action="" className="form mb-3" onSubmit={handleSubmit}>
            <span className="search-icon">⌕</span>
            <input
                type="search"
                placeholder="Adanel"
                className={`search-input ${
                    errors.search ? 'is-invalid' : ''
                }`}
                value={query}
                onChange={handleChange}
                data-testid="search"
            />

            {errors.search ? (
                <div
                    className="search-input__error-text text-center color-danger"
                    data-testid="error"
                >
                    <small>{errors.search}</small>
                </div>
            ) : null}

            <button
                type="submit"
                className="button search-button"
                data-testid="submit"
            >
                Search
            </button>
        </form>
    );
}
