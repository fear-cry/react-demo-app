import React, { memo } from 'react';

function SortBy(props) {
    const { sortBy, setSortBy } = props;

    const handleInputChange = (e) => {
        const { value } = e.target;

        setSortBy(value);
    };

    return (
        <div className="form-check form-check-inline color-white">
            <span className="mr-4">Sort by:</span>

            <label className="form-check-label mr-2" htmlFor="byName">
                <input
                    className="form-check-input"
                    type="radio"
                    name="sortBy"
                    id="byName"
                    value="byName"
                    checked={sortBy === 'name'}
                    onChange={handleInputChange}
                    data-testid="sort-by-name"
                />
                name
            </label>
            <label className="form-check-label mr-2" htmlFor="race">
                <input
                    className="form-check-input"
                    type="radio"
                    name="sortBy"
                    id="race"
                    value="race"
                    checked={sortBy === 'race'}
                    onChange={handleInputChange}
                />
                race
            </label>
            <label className="form-check-label mr-2" htmlFor="spouse">
                <input
                    className="form-check-input"
                    type="radio"
                    name="sortBy"
                    id="spouse"
                    value="spouse"
                    checked={sortBy === 'spouse'}
                    onChange={handleInputChange}
                />
                spouse
            </label>
        </div>
    );
}

export default memo(SortBy);
