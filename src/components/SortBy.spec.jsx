import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import SortBy from './SortBy';

describe('<SortBy />', () => {
    const RADIO_SORT_BY_NAME_ID = 'sort-by-name';

    it('Renders <SortBy /> component correctly', () => {
        const { getByText } = render(<SortBy />);

        expect(getByText(/Sort by/i)).toBeInTheDocument();
    });

    it('Type radio input', () => {
        render(<SortBy />);

        const radioEl = screen.getByTestId(RADIO_SORT_BY_NAME_ID);
        expect(radioEl).toBeInTheDocument();
        expect(radioEl).toHaveAttribute('type', 'radio');
    });

    it('set sorting radio input', () => {
        const setSortBy = jest.fn();
        render(<SortBy setSortBy={setSortBy} />);

        const radioEl = screen.getByTestId(RADIO_SORT_BY_NAME_ID);
        userEvent.click(radioEl);

        expect(setSortBy).toBeCalled();
    });

    it('Checked radio input', () => {
        const sortBy = 'name';

        render(<SortBy sortBy={sortBy} />);

        const radioEl = screen.getByTestId(RADIO_SORT_BY_NAME_ID);
        expect(radioEl.checked).toBe(true);
    });
});
