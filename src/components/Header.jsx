import React from 'react';
import { NavLink } from 'react-router-dom';

export default function Header({ routes }) {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="/">
                React.routing
            </a>

            <div
                className="navbar-collapse"
                id="navbarSupportedContent"
            >
                <ul className="navbar-nav mr-auto">
                    {routes.map((route) => (
                        <NavLink
                            className="nav-link"
                            key={route.path}
                            to={route.path}
                            activeClassName="active"
                            exact
                        >
                            {route.name}
                            <span className="sr-only">(current)</span>
                        </NavLink>
                    ))}
                </ul>
            </div>
        </nav>
    );
}
