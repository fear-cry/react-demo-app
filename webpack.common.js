const path = require('path');
const glob = require('glob-all');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const LEVELS = glob
    .sync('./src/**')
    .map((level) => path.resolve(`./${level}`));

const config = {
    entry: './src/index.jsx',

    module: {
        rules: [
            {
                test: /\.jsx?$/i,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',
                            '@babel/preset-react',
                        ],
                    },
                },
            },
            {
                test: /\.(scss|css)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ],
            },
        ],
    },
    resolve: {
        extensions: ['.jsx', '.js', '...'],
        modules: [
            path.resolve(__dirname, 'src'),
            'node_modules',
            ...LEVELS,
        ],
        alias: {
            '@': path.resolve(__dirname, 'src'),
        },
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        clean: true,
        publicPath: '/',
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Production',
            template: 'src/index.html',
        }),

        new ESLintPlugin({
            fix: true,
            failOnError: true,
            lintDirtyModulesOnly: false,
        }),

        new MiniCssExtractPlugin(),
    ],
};

module.exports = config;
