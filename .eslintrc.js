const path = require('path');

module.exports = {
    root: true,
    parser: 'babel-eslint',
    parserOptions: {
        ecmaVersion: 11,
        sourceType: 'module',
        allowImportExportEverywhere: true,
    },
    env: {
        browser: true,
        node: true,
        es6: true,
        jquery: true,
        es2020: true,
        jest: true,
    },
    plugins: ['prettier', 'react-hooks', 'import'],
    extends: [
        'airbnb',
        'airbnb/hooks',
        'prettier',
        'plugin:import/errors',
    ],
    rules: {
        'react-hooks/rules-of-hooks': 'error',
        'react-hooks/exhaustive-deps': 'off',
        'react/prop-types': 'off',
        'prettier/prettier': 'error',
        'no-unused-vars': 'warn',
        'no-console': 'off',
        'func-names': 'off',
        'no-process-exit': 'off',
        'object-shorthand': 'off',
        'class-methods-use-this': 'off',
    },
    settings: {
        react: {
            pragma: 'React',
            version: 'detect',
        },
        ecmascript: 6,
        jsx: true,
        'import/resolver': {
            webpack: {
                config: path.join(__dirname, 'webpack.dev.js'),
            },
        },
    },
};
